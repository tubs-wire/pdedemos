function test_iterative
%#ok<*AGROW>

degree = pi/180;
A = test_matrix([1, 6], 30*degree);

while true
    x = [3; 2];
    b = A * x;
    hold off;
    plot_contours(A, b, [-5, 20], [-5, 15]);
    grid on;
    axis equal;
    hold all;
    
    [V, D] = eig(A);
    v=20*V(:,1);
    P = [x-v, x+v];
    line(P(1,:), P(2,:));
    v=20*V(:,2);
    P = [x-v, x+v];
    line(P(1,:), P(2,:));
    
    if ~exist('mx', 'var')
        [mx, my] = ginput(1);
    end
    x0 = [mx; my];
    
    
    for n=0:10
        p = jacobi(A, b, x0, n);
        plot(p(1,:), p(2,:), 'r-x')
        
        p = gauss_seidel(A, b, x0, n);
        plot(p(1,:), p(2,:), 'b-o')
        
        p = steepest_descent(A, b, x0, n);
        plot(p(1,:), p(2,:), 'g-o')
        
        p = conjugate_gradients(A, b, x0, n);
        plot(p(1,:), p(2,:), 'k-*')
        
        pause(0.1);
    end

    legend('energy functional', '', '', 'jacobi', 'gauss seidel', 'steepest descent', 'conjugate gradients');
    
    hold off;
    [mx, my, b] = ginput(1);
    if b~=1
        break
    end
end

function p=jacobi(A, b, x, N)
p=x;
D = diag(diag(A));
for i=1:N
    x = x + D \ (b - A * x);
    p=[p, x];
end

function p=gauss_seidel(A, b, x, N)
p=x;
P = triu(A, 0);
for i=1:N
    x = x + P \ (b - A * x);
    p=[p, x];
end

function p=steepest_descent(A, b, x, N)
p=x;
for i=1:N
    r = b - A * x;
    d = r;
    alpha = r' * d / (d' * A * d);
    x = x + alpha * d;
    p=[p, x];
end

function p=conjugate_gradients(A, b, x, N)
p=x;
for i=1:N
    r = b - A * x;
    rho1 = r' * r;
    if i==1
        d = r;
    else
        beta = rho1 / rho2;
        beta
        d = r + beta * d;
    end
    alpha = r' * d / (d' * A * d);
    x = x + alpha * d;
    p=[p, x];
    rho2 = rho1;
end

function plot_contours(A, b, mx, my)
if nargin<3
    my=mx;
end
N = 100;
x = make_points(mx, N);
y = make_points(my, N);
[X, Y]=meshgrid(x, y);
Z = grid_eval(A, b, X, Y);
%contour(X, Y, sqrt(Z-min(Z(:))), 30);
contour(X, Y, Z, 30);


function Z=grid_eval(A, b, X, Y)
s = size(X);
P = [X(:), Y(:)]';
Z = 0.5*sum(P.*(A*P),1) - b(:)' * P;
Z = reshape(Z, s);


function x=make_points(mx, N)
if length(mx)==1
    x = linspace(-mx, mx, N);
else
    x = linspace(mx(1), mx(2), N);
end

function A=test_matrix(ev, alpha)
s = sin(alpha);
c = cos(alpha);
U = [c, s; -s, c];
D = diag(ev);
A=U'*D*U;


